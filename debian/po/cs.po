# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: havp\n"
"Report-Msgid-Bugs-To: havp@packages.debian.org\n"
"POT-Creation-Date: 2014-08-22 22:08+0200\n"
"PO-Revision-Date: 2007-05-08 10:14+0200\n"
"Last-Translator: Miroslav Kure <kurem@debian.cz>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../havp.templates:1001
msgid "Do you want to create a loopback spool file system?"
msgstr "Chcete vytvořit souborový systém pro frontu na lokální smyčce?"

#. Type: boolean
#. Description
#: ../havp.templates:1001
msgid ""
"HAVP strictly requires the file system where it stores its temporary files "
"during scanning to support mandatory locking. Many of the standard Linux "
"file systems support this, but do not enable it by default."
msgstr ""
"HAVP striktně vyžaduje, aby souborový systém, na který si během prohledávání "
"ukládá dočasné soubory, podporoval vynucené zamykání. Mnoho standardních "
"linuxových souborových systémů to sice podporuje, ale standardně to nebývá "
"zapnuto."

#. Type: boolean
#. Description
#: ../havp.templates:1001
msgid ""
"To use HAVP, you can either mount the file system that contains /var/spool/"
"havp with the option \"mand\", or create a loopback file system that is "
"mounted at /var/spool/havp only for HAVP."
msgstr ""
"Abyste mohli HAVP používat, musíte buď připojit souborový systém, který "
"obsahuje /var/spool/havp s parametrem \"mand\", nebo můžete vytvořit "
"souborový systém připojený na lokální smyčce jako /var/spool/havp, který "
"bude sloužit jenom pro HAVP."

#. Type: boolean
#. Description
#: ../havp.templates:1001
msgid ""
"If you are in doubt, you should accept this option to create a loopback "
"spool file system."
msgstr ""
"Jste-li na pochybách, měli byste odpovědět kladně a vytvořit souborový "
"systém pro frontu souborů na lokální smyčce."

#. Type: string
#. Description
#: ../havp.templates:2001
msgid "Loopback file system size:"
msgstr "Velikost souborového systému na lokální smyčce:"

#. Type: string
#. Description
#: ../havp.templates:2001
msgid ""
"Please enter the size (in megabytes) of the loopback file system to be "
"created."
msgstr ""
"Zadejte prosím velikost (v megabajtech) souborového systému na lokální "
"smyčce, který se má vytvořit."
